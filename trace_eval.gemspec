# frozen_string_literal: true

require_relative 'lib/trace_eval/version'

Gem::Specification.new do |spec|
  spec.name          = 'trace_eval'
  spec.version       = TraceEval::VERSION
  spec.authors       = ['Frank J. Cameron']
  spec.email         = ['fjc@fastmail.net']

  spec.summary       = 'Evaluates the Ruby expression(s) in string and prints each executing line.'
  spec.homepage      = 'https://gitlab.com/fjc/trace_eval'
  spec.license       = 'MIT'
  spec.required_ruby_version = '>= 2.7.0'

  spec.metadata['homepage_uri'] = spec.homepage

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) {
    `git ls-files -z`.split("\x0").reject do |f|
      (f == __FILE__) || f.match(%r{\A(?:(?:test|spec|features)/|\.(?:git|travis|circleci)|appveyor)})
    end
  }

  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']
end
