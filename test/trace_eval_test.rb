# frozen_string_literal: true

require 'test_helper'

class TraceEvalTest < Minitest::Test
  def test_that_it_has_a_version_number
    refute_nil ::TraceEval::VERSION
  end

  def test_it_does_something_useful
    extend TraceEval

    $stderr, _stderr = StringIO.new, $stderr
    assert_equal trace_eval("2+2\n"), 4
    $stderr, _stderr = _stderr, $stderr
    assert_equal _stderr.string, "2+2\n"
  end
end
