# TraceEval

Evaluates the Ruby expression(s) in string and prints each executing line.

The standard ruby `tracer` does not display the source inside an `eval`:

```
$ ruby -rtracer foo.rb 
#0:foo.rb:1::-: puts 1+2
3
#0:foo.rb:2::-: eval "puts 3+4\nputs 5+6\n"
#0:(eval):1::-: -
7
#0:(eval):2::-: -
11
#0:foo.rb:3::-: puts 7+8
15
```

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'trace_eval'
```

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install trace_eval

## Usage

```
$ ./bin/console 
irb(main):001:0> extend TraceEval
=> main
irb(main):002:0> trace_eval "a = 2\nb = 2\nputs a+b\n"
a = 2
b = 2
puts a+b
4
=> nil
```

### Bugs

If the traced eval in turn evals, the trace will be nonsense:

```
irb(main):001:0> extend TraceEval
=> main
irb(main):002:0> trace_eval "0+1\neval \"2+3\n4+5\n\"\n6+7\n"
0+1
eval "2+3
0+1
eval "2+3
6+7
=> 13
```

Nested `trace_eval` does work:

```
irb(main):001:0> extend TraceEval
=> main
irb(main):002:0> trace_eval "0+1\ntrace_eval \"2+3\n4+5\n\"\n6+7\n"
0+1
trace_eval "2+3
2+3
4+5
6+7
=> 13
```

## Concept of Operation

- Save a random value in a magic variable to distinguish this eval
  from others
- Save an array of the lines of the eval string
- Set a `TracePoint` for `:line` events (execute code on a new line)
- When the trace point is activated
  - Only proceed if the line's `path` is '(eval)'
  - Only proceed if the magic variable matches the expected value
  - Print the appropriate line from the array by `lineno`

## Development

After checking out the repo, run `bin/setup` to install
dependencies. Then, run `rake test` to run the tests. You can also run
`bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake
install`. To release a new version, update the version number in
`version.rb`, and then run `bundle exec rake release`, which will create
a git tag for the version, push git commits and the created tag, and
push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitLab at
https://gitlab.com/fjc/trace_eval.

## License

The gem is available as open source under the terms of the [MIT
License](https://opensource.org/licenses/MIT).
