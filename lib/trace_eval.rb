# frozen_string_literal: true

require_relative 'trace_eval/version'

# Use trace point to print lines of code during eval.  A randomized
# sigil value is used to only print lines from this invocation of eval,
# but nonsense could still ensure if this eval also evals.
#
module TraceEval
  def trace_eval(code)
    __wzpxmnrjafemvlrwflblbbyenywk__ = rand
    lines = [
      "__wzpxmnrjafemvlrwflblbbyenywk__ = #{__wzpxmnrjafemvlrwflblbbyenywk__}\n"
    ] + code.lines

    tp = TracePoint.new(:line) do |tp|
      next unless tp.path == '(eval)'
      next unless tp.binding.local_variable_get(
        :__wzpxmnrjafemvlrwflblbbyenywk__
      )==__wzpxmnrjafemvlrwflblbbyenywk__
      $stderr.print lines[tp.lineno]
    rescue NameError
    end

    begin
      tp.enable
      eval code
    ensure
      tp.disable
    end
  end
end
